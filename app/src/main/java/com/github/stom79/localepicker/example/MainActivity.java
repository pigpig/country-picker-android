package com.github.stom79.localepicker.example;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.github.stom79.localepicker.Country;
import com.github.stom79.localepicker.CountryPicker;
import com.github.stom79.localepicker.CountryPickerListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

  private TextView mCountryNameTextView, mCountryLocaleTextView;
  private ImageView mCountryFlagImageView;
  private Button mPickCountryButton;
  private CountryPicker mCountryPicker;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    initialize();
    setListener();
  }

  private void setListener() {
    mCountryPicker.setListener(new CountryPickerListener() {
      @Override
      public void onSelectCountry(String name, String locale,
                                  int flagDrawableResID) {
        mCountryNameTextView.setText(name);
        mCountryLocaleTextView.setText(locale);
        mCountryFlagImageView.setImageResource(flagDrawableResID);
        mCountryPicker.dismiss();
      }
    });
    mPickCountryButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mCountryPicker.show(getSupportFragmentManager(), "LOCALE_PICKER");
      }
    });
    getUserCountryInfo();
  }

  private void initialize() {
    mCountryNameTextView = findViewById(R.id.selected_country_name_text_view);
    mCountryLocaleTextView = findViewById(R.id.selected_country_locale_text_view);
    mPickCountryButton = findViewById(R.id.country_picker_button);
    mCountryFlagImageView = findViewById(R.id.selected_country_flag_image_view);
    mCountryPicker = CountryPicker.newInstance("Select Country");

    // You can limit the displayed countries
    ArrayList<Country> nc = new ArrayList<>();
    String[] allowedLocale = new String[]{"de","en","fr","hu","nl","ru","zh"};
    for (Country c : Country.getAllCountries()) {
      if (Arrays.asList(allowedLocale).contains(c.getLocale())) {
        nc.add(c);
      }
    }
    // and decide, in which order they will be displayed
    Collections.reverse(nc);
    mCountryPicker.setCountriesList(nc);
  }

  private void getUserCountryInfo() {
    Country country = Country.getCountryFromSIM(getApplicationContext());
    if (country != null) {
      mCountryFlagImageView.setImageResource(country.getFlag());
      mCountryNameTextView.setText(country.getName());
      mCountryLocaleTextView.setText(country.getLocale());
    }
  }
}